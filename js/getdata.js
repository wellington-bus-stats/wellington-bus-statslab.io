function status(text) {
    document.getElementById("status").innerText = text;
}

var rti;
var data;

function downloadData() {
    status("Downloading data...");
    data = new XMLHttpRequest();
    data.onreadystatechange = function () {
        var DONE = this.DONE || 4;
        if (this.readyState === DONE){
            status("Decompressing...");
            window.setTimeout(decompressAndShowGraphs, 20);
        }
    };
    data.open("GET", "data/data.json.gz", true);
    data.responseType = "arraybuffer";
    data.send(null);
}

function decompressAndShowGraphs() {
    var array = new Uint8Array(data.response);
    var jsonData = new TextDecoder("utf-8").decode(pako.inflate(array));
    jsonData = '[' + jsonData.replace(/^\]\}$/gm, "]},").slice(0, -2)  + ']';
    rti = JSON.parse(jsonData);
    status("Computing...");
    window.setTimeout(function() { showGraphs(rti) }, 20);
}
