window.addEventListener('load', function () {
    for (x of document.getElementsByClassName("tablinks"))
        x.addEventListener("click", function (event) {
            switchTab(event, event.target.attributes["tabname"].value);
        });

    let form = document.getElementById("locationfilterform");
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        locationGraphRoute();
    });

    window.addEventListener("keydown", function (event) {
        switch (event.key) {
        case "Down": case "ArrowDown":
            u_offy -= 1;
            break;
        case "Up": case "ArrowUp":
            u_offy += 1;
            break;
        case "Left": case "ArrowLeft":
            u_offx -= 1;
            break;
        case "Right": case "ArrowRight":
            u_offx += 1;
            break;
        default:
            return;
        }
        locationGraphRoute();
        event.preventDefault();
    });

    status("Loading...");

    window.setTimeout(downloadData, 20);
});
