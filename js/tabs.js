function switchTab(event, id) {
    tabc = document.getElementsByClassName("tabc");
    for (let i = 0; i < tabc.length; i++) {
        tabc[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (let i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(id).style.display = "block";
    event.currentTarget.className += " active";
}
