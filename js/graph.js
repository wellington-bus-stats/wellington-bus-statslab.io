function setValue(id, value) {
    let elem = document.getElementById(id);
    elem.innerText = value;
}

function setInt(id, value) {
    setValue(id, Math.floor(value));
}

function setPercent(id, value) {
    setInt(id, value*100);
}

function tableAdd(table, strings, rtype='td') {
    let tr = document.createElement('tr');

    for (let x of strings) {
        let td = document.createElement(rtype);
        td.innerText = x;
        tr.appendChild(td);
    }

    table.appendChild(tr);
}

function tableHeader(table, strings) {
    tableAdd(table, strings, 'th');
}

var u_offx = 0;
var u_offy = 0;

function showLatestRoute(count, late, secs) {
    let larr = [];
    for (let key in late) {
        if (!count[key] ||
            count[key] < 30)
            continue;
        let cr = late[key] / count[key];
        larr.push([key, cr]);
    }
    larr.sort(function(a, b) {
        return b[1] - a[1];
    });
    setValue("latestroute", larr[0][0]);
    setPercent("latestroutepercent", larr[0][1]);
    setInt("latestrouteseconds", secs[larr[0][0]] / count[larr[0][0]]);
    let table = document.getElementById("routelatenesstable");
    tableHeader(table, ["Route:", "Percent late:", "Avg time late:"]);
    for (let i = 0; i < 20 && i < larr.length; ++i) {
        let time = Math.floor(secs[larr[i][0]] / count[larr[i][0]]);
        time = Math.round(time/10)*10;
        if (time > 60)
            time = Math.floor(time/60) + "m " + time%60;
        time += "s";
        tableAdd(table, [
            larr[i][0],
            Math.floor(larr[i][1]*100) + "%",
            time
        ]);
    }
}

function drawCircle(ctx, x, y, diam) {
    ctx.moveTo(x, y);
//    if (diam < 3)
        ctx.rect(x, y, diam, diam);
//    else
//        ctx.arc(x, y, diam, 0, 2*Math.PI);
}

function locationGraph(locations, alpha = 0.5) {
    if (!locations.length)
        return;
/*    let latmin = locations[0][0];
    let lngmin = locations[0][1];
    let latmax = latmin;
    let lngmax = lngmin;
    for (x of locations) {
        if (x[0] < latmin)
            latmin = x[0];
        if (x[0] > latmax)
            latmax = x[0];
        if (x[1] < lngmin)
            lngmin = x[1];
        if (x[1] > lngmax)
            lngmax = x[1];
    }*/
    const canvas = document.getElementById('location-canvas');
    const ctx = canvas.getContext('2d');
/*    let scalex = canvas.width / Math.abs(latmax - latmin);
    let scaley = canvas.height / Math.abs(lngmax - lngmin);
    let scale = Math.min(scalex, scaley);
    let offx = latmin;
    let offy = lngmin;*/
    //    locations = locations.slice(0, 20000);

    ctx.save();
    ctx.scale(0.4, 0.4);
    ctx.scale(0.85, 0.63);

//    let img = document.getElementById('mapimg');
//    ctx.drawImage(img, -900, -900);

    ctx.restore();
    ctx.save();

    let scale = 2183.3*2;
    // Whoops, I got x and y mixed up here...
    let offx = -40.7357941 - 0.36642 - 0.1 + u_offy/100;
    let offy = 174.7213593 - 0.04580 + u_offx/100;
    if (alpha == -1)
        ctx.strokeStyle = '#ff0000';
    else
        ctx.strokeStyle = 'rgba(0, 0, 0, ' + alpha + ')';

    // Some browsers require using a separate stroke for each circle
    //     as otherwise it just shows a flat grey,
    //     but that slows it down a *lot* for other browsers.
    // How about doing some fancy sorting so only objects that don't
    //     overlap are drawn per stroke?
    // Doing a fill operation might be faster than a stroke, but
    //     I'd need to do the above fancy sorting first.
    ctx.beginPath();
    for (let x of locations) {
        drawCircle(ctx, (x[1]-offy)*scale, (offx-x[0])*scale, 1);
    }
    ctx.stroke();

    ctx.restore();
}

function historyGraph(rti) {
/*    let weekdays = [
        "Sun ",
        "Mon ",
        "Tue ",
        "Wed ",
        "Thu ",
        "Fri ",
        "Sat "
        ];
*/

    let weekdays = [ "S ", "M ", "T ", "W ", "T ", "F ", "S " ];

    let cp = {};
    let qp = {};
    let np = {};

    for (let x of rti) {
        let d = x.Date.substring(0, 10);
        let count = 0;
        let late = 0;
        for (let y of x.Data) {
            for (let z of y.Locations.Services) {
                ++count;
                if (z.BehindSchedule)
                    ++late;
            }
        }
        if (!cp[d])
            cp[d] = 0;
        cp[d] += count;
        if (!qp[d])
            qp[d] = 0;
        qp[d] += late;
    }

    const canvas = document.getElementById('history-canvas');
    const ctx = canvas.getContext('2d');
    ctx.lineWidth = 5;

    let a = [];
    for (let x in cp)
        a.push([cp[x], qp[x], x]);

    for (type of [0, 1]) {
        let n = a.length;
        let i = 0;
        let d = canvas.width/n;
        let vmul = -canvas.height*1.5;
        let vadd = canvas.height*1.5-100;

        if (type) {
            ctx.strokeStyle = '#7f7f7f7f';
        }

        ctx.moveTo(0, a[0][1]/a[0][0]*vmul+vadd);
        ctx.textAlign = "center";
        ctx.lineCap = "round";
        let count = 0;
        ctx.font = "15px Serif";
        let avg = 0;
        let txtcount = 0;
        let pos = a[0][1]/a[0][0]*vmul+vadd;
        if (type)
            pos = a[0][1]/1000*vmul+vadd;

        for (let x of a) {

            ++count;
            let dateobj = new Date(x[2]);

            if (type)
                pos = x[1] / 5000 * vmul + vadd / 2;
            else
                pos = x[1] / x[0] * vmul + vadd;

            if (dateobj.getDay() == 1 /* Monday */) {
                ctx.stroke();
                ctx.beginPath();
                ctx.moveTo(i, pos)
            }
            else
                ctx.lineTo(i, pos)

            avg += x[1]/x[0];
            ++txtcount;
            if (!type) {
                let val = Math.floor(avg/txtcount*100);
                avg = txtcount = 0;
                ctx.rotate(Math.PI/2);
                let date = x[2].substring(5);
                date = date.substring(3)+'/'+date.substring(0,2);
                // date = weekdays[dateobj.getDay()] + date;
                ctx.fillText(date, canvas.height/2-100, -i+d/4);
                ctx.fillText(val + "%", canvas.height-150, -i+d/4);
                ctx.rotate(-Math.PI/2);
            }
            i += d;
        }
        ctx.stroke();
        ctx.beginPath();

    }
}

function locationGraphRoute() {
    const canvas = document.getElementById('location-canvas');
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    const route = document.getElementById('map-routesel').value;
    let locations = new Array();
    let restLocations = new Array();
    for (x of rti) {
        for (y of x.Data) {
            if (y.Route == route || y.Route == route+".1") {
                for (z of y.Locations.Services) {
                    let lat = z.Lat;
                    let lng = z.Long;
                    if (lat && lng)
                        locations.push([lat, lng]);

                }
            }
            else {
                for (z of y.Locations.Services) {
                    let lat = z.Lat;
                    let lng = z.Long;
                    if (lat && lng)
                        restLocations.push([lat, lng]);
                }
            }
        }
    }
    if (route == "")
        locationGraph(restLocations);
    else {
        locationGraph(restLocations, 0.3);
        locationGraph(locations, -1);
    }
}

function showGraphs(rti) {
    let count = 0;
    let late = 0;
    let delay = 0;

    let countper = new Object();
    let lateper = new Object();
    let delayper = new Object();

    let locations = new Array();

    for (i = 0; i < rti.length; ++i) {
        let p = rti[i].Data;
        for (j = 0; j < p.length; ++j) {
            let q = p[j].Locations.Services;
            let route = p[j].Route;
            for (k = 0; k < q.length; ++k) {
                let lat = q[k].Lat;
                let lng = q[k].Long;
                if (lat && lng)
                    locations.push([lat, lng]);
                delay += q[k].DelaySeconds;
                if (!delayper[route])
                    delayper[route] = 0;
                delayper[route] += q[k].DelaySeconds;
                ++count;
                if (countper[route])
                    ++countper[route];
                else
                    countper[route] = 1;
                if (q[k].BehindSchedule) {
                    ++late;
                    if (lateper[route])
                        ++lateper[route];
                    else
                        lateper[route] = 1;
                }
            }
        }
    }
    setPercent("percentlate", late/count);
    setInt("avgsecondslate", delay/count);
    showLatestRoute(countper, lateper, delayper);
    historyGraph(rti);
    locationGraph(locations);
    status("Wellington Bus Stats");
}
